package de.unipassau.fim.webeng2019.studiqa.application.service;

import de.unipassau.fim.webeng2019.studiqa.application.repositories.QuestionRepository;
import de.unipassau.fim.webeng2019.studiqa.model.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionService {

    private final QuestionRepository questionRepository;

    @Autowired
    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public Question getDemoQuestion() {
        return questionRepository.getFirstById((long) 1337);
    }

}
