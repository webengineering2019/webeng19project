package de.unipassau.fim.webeng2019.studiqa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudiqaApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudiqaApplication.class, args);
	}

}
