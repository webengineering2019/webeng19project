package de.unipassau.fim.webeng2019.studiqa.web.controller;

import de.unipassau.fim.webeng2019.studiqa.application.service.QuestionService;
import de.unipassau.fim.webeng2019.studiqa.model.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/questions")
public class QuestionController {

    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping(path = "/demo")
    @ResponseBody
    public Question demo() {
        return questionService.getDemoQuestion();
    }
}
