package de.unipassau.fim.webeng2019.studiqa.application.repositories;

import de.unipassau.fim.webeng2019.studiqa.model.Question;
import org.springframework.data.repository.CrudRepository;

public interface QuestionRepository extends CrudRepository<Question, Long> {
    Question getFirstById(Long id);
}
