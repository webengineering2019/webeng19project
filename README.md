# StudiQA

StudiQA is a simple web application to ask questions and get an answer from any registered user.
This is a project within the lecture Web and Data Engineering in summer semester 2019.

## Project Members
- Marco Wintersehl
- Patrick Schuster
- Alexander Reißig
- Barbara Lutz
- Jan von Aschwege

## Functionality
- [ ] User Management (Log In/Sign Up)
- [ ] Submitting a question
- [ ] Submitting an answer
- [ ] Accepting an answer
- [ ] Filter all questions
- [ ] Bookmarking questions

## Notice
This project uses H2's in-memory database, all data are not persistent. 
When editing the source code, make sure you have the Plugin for [Lombok](https://projectlombok.org/) installed. 
It automatically creates getter/setter for data classes. 